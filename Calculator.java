import org.eclipse.jface.dialogs.MessageDialog;



public class Calculator {
	private DataBindingContext m_bindingContext;

	protected Shell shell;
	private final FormToolkit formToolkit = new FormToolkit(
			Display.getDefault());
	private Text TextInput;
	String operation;
	String firstnum;
	int secondnum;
	
		
	String operationEqul(String first, int second, String oper) {
		String result = "";
			
			if ((oper == "+")) {
				result = Integer.toString(Integer.parseInt(first) + second);
				}
			
			if ((oper == "-") ) {
				result = Integer.toString(Integer.parseInt(first) - second);
				}
			
			if ((oper == "*") ) {
				result = Integer.toString(Integer.parseInt(first) * second);
				}
			
			if ((oper == "/") ) {
				result = Float.toString((float)Integer.parseInt(first) / second);
				}
			if ((oper == "^") ) {
				result = Long.toString((long) Math.pow(Integer.parseInt(first), second));
				}
					
		return result;		
	}
	
	
	long factorial(int n){
	        return ( n==0 ? 1 : n*factorial(n-1) );
	    }
	
	String sqrtAndfactorial(String first, String oper){
		String result = "";
		double r;
		if (oper == "!"){
			result = Long.toString(factorial(Integer.parseInt(first)));
			}
		if (oper == "sqrt"){
			result = Double.toString(Math.sqrt(Double.parseDouble(first)));}
			if (result == "NaN") {
				result = "";
			}
		if (oper == "sin"){
			r = Math.sin(Math.toRadians(Double.parseDouble(first)));
			r = r*1000;
			r = (int)Math.round(r);
			r = r/1000;
			result = Double.toString(r);
			}
		if (oper == "cos"){
			r = Math.cos(Math.toRadians(Double.parseDouble(first)));
			r = r*1000;
			r = (int)Math.round(r);
			r = r/1000;
			result = Double.toString(r);
			}
		if (oper == "tg"){
			r = Math.tan(Math.toRadians(Double.parseDouble(first)));
			r = r*1000;
			r = (int)Math.round(r);
			r = r/1000;
			result = Double.toString(r);
			}
		if (oper == "ctg"){
			r = Math.cos(Math.toRadians(Double.parseDouble(first)))/Math.sin(Math.toRadians(Double.parseDouble(first)));
			r = r*1000;
			r = (int)Math.round(r);
			r = r/1000;
			result = Double.toString(r);
		}
		
		if (oper == "areaCube"){
			result = Double.toString(6*Double.parseDouble(first)*Double.parseDouble(first));
		}
		
		if (oper == "volOfcube"){
			result = Double.toString(Double.parseDouble(first)*Double.parseDouble(first)*Double.parseDouble(first));
		}
		
		if (oper == "perOfCube"){
			result = Double.toString(12*Double.parseDouble(first));
		}
					
		return result;
		}
		
		
	
	
	/**
	 * Launch the application.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		Display display = Display.getDefault();
		Realm.runWithDefault(SWTObservables.getRealm(display), new Runnable() {
			public void run() {
				try {
					Calculator window = new Calculator();
					window.open();
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Open the window.
	 */
	public void open() {
		Display display = Display.getDefault();
		createContents();
		shell.open();
		shell.layout();
		while (!shell.isDisposed()) {
			if (!display.readAndDispatch()) {
				display.sleep();
			}
		}
	}

	/**
	 * Create contents of the window.
	 */
	protected void createContents() {
		shell = new Shell();
		shell.setModified(true);
		shell.setSize(434, 300);
		shell.setText("Калькулятор");
		
		TextInput = formToolkit.createText(shell, "New Text", SWT.READ_ONLY | SWT.RIGHT);
		TextInput.setFont(SWTResourceManager.getFont("Segoe UI", 11, SWT.NORMAL));
		TextInput.setText("");
		TextInput.setBounds(5, 18, 279, 28);
		
		final Label labelInput = new Label(shell, SWT.RIGHT);
		labelInput.setFont(SWTResourceManager.getFont("Comic Sans MS", 7, SWT.NORMAL));
		labelInput.setBounds(5, 2, 279, 15);
		formToolkit.adapt(labelInput, true, true);
		m_bindingContext = initDataBindings(); 		
		
		Button ButtonPlus = formToolkit.createButton(shell, "+", SWT.NONE);
		ButtonPlus.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try{ 
					firstnum = TextInput.getText();
					TextInput.setText("");
					labelInput.setText(firstnum + " + ");
					operation = "+";}
				
				catch (Exception exc){
					MessageDialog.openError(shell, "Error", "Возможно вы не ввели число или оно слишком большое");
					
				}
			}
		});
		ButtonPlus.setBounds(160, 94, 60, 35);
		
		Button ButtonEqual = formToolkit.createButton(shell, "=", SWT.NONE);
		ButtonEqual.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
			try {
				if (TextInput.getText() != "" && operation != "=") {
				secondnum = Integer.parseInt(TextInput.getText());
				labelInput.setText(labelInput.getText() + secondnum + " = " + operationEqul(firstnum, secondnum, operation));
				TextInput.setText(operationEqul(firstnum, secondnum, operation));
				operation = "=";
			}
			}
			catch (Exception exc){
				}
			}
		});
		ButtonEqual.setBounds(224, 52, 60, 35);
		
		Button BtnNum7 = formToolkit.createButton(shell, "7", SWT.NONE);
		BtnNum7.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try{
					if (operation == "="){
						TextInput.setText("");
						operation = "";
						}
					
					TextInput.setText(TextInput.getText()+"7");
				}
				catch (Exception exc){
					MessageDialog.openError(shell, "Error", "Ops");
				}
			}
		});
		BtnNum7.setBounds(10, 67, 40, 40);
		
		Button BtnNum4 = formToolkit.createButton(shell, "4", SWT.NONE);
		BtnNum4.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try{
					if (operation == "="){
						TextInput.setText("");
						operation = "";
						}
					TextInput.setText(TextInput.getText()+"4");
				}
				catch (Exception exc){
					MessageDialog.openError(shell, "Error", "Ops");
				}
			}
		});
		BtnNum4.setBounds(10, 110, 40, 40);
		
		Button BtnNum1 = formToolkit.createButton(shell, "1", SWT.NONE);
		BtnNum1.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try{
					if (operation == "="){
						TextInput.setText("");
						operation = "";
						}
					TextInput.setText(TextInput.getText()+"1");
				}
				catch (Exception exc){
					MessageDialog.openError(shell, "Error", "Ops");
				}
			}
			
		});
		BtnNum1.setBounds(10, 154, 40, 40);
		
		Button BtnNum8 = formToolkit.createButton(shell, "8", SWT.NONE);
		BtnNum8.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try{
					if (operation == "="){
						TextInput.setText("");
						operation = "";
						}
					TextInput.setText(TextInput.getText()+"8");
				}
				catch (Exception exc){
					MessageDialog.openError(shell, "Error", "Ops");
				}
			}
		});
		BtnNum8.setBounds(56, 67, 40, 40);
		
		Button BtnNum5 = formToolkit.createButton(shell, "5", SWT.NONE);
		BtnNum5.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try{
					if (operation == "="){
						TextInput.setText("");
						operation = "";
						}
					TextInput.setText(TextInput.getText()+"5");
				}
				catch (Exception exc){
					MessageDialog.openError(shell, "Error", "Ops");
				}
			}
		});
		BtnNum5.setBounds(56, 110, 40, 40);
		
		Button BtnNum2 = formToolkit.createButton(shell, "2", SWT.NONE);
		BtnNum2.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try{
					if (operation == "="){
						TextInput.setText("");
						operation = "";
						}
					TextInput.setText(TextInput.getText()+"2");
				}
				catch (Exception exc){
					MessageDialog.openError(shell, "Error", "Ops");
				}
			}
		});
		BtnNum2.setBounds(56, 154, 40, 40);
		
		Button BtnNum9 = formToolkit.createButton(shell, "9", SWT.NONE);
		BtnNum9.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try{
					if (operation == "="){
						TextInput.setText("");
						operation = "";
						}
					TextInput.setText(TextInput.getText()+"9");
				}
				catch (Exception exc){
					MessageDialog.openError(shell, "Error", "Ops");
				}
			}
		});
		BtnNum9.setBounds(102, 67, 40, 40);
		
		Button BtnNum6 = formToolkit.createButton(shell, "6", SWT.NONE);
		BtnNum6.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try{
					if (operation == "="){
						TextInput.setText("");
						operation = "";
						}
					TextInput.setText(TextInput.getText()+"6");
				}
				catch (Exception exc){
					MessageDialog.openError(shell, "Error", "Ops");
				}
			}
		});
		BtnNum6.setBounds(102, 110, 40, 40);
		
		Button BtnNum3 = formToolkit.createButton(shell, "3", SWT.NONE);
		BtnNum3.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try{
					if (operation == "="){
						TextInput.setText("");
						operation = "";
						}
					TextInput.setText(TextInput.getText()+"3");
				}
				catch (Exception exc){
					MessageDialog.openError(shell, "Error", "Ops");
				}
			}
		});
		BtnNum3.setBounds(102, 154, 40, 40);
		
		Button BtnNum0 = formToolkit.createButton(shell, "0", SWT.NONE);
		BtnNum0.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try{
					if (operation == "="){
						TextInput.setText("");
						operation = ""; 
						}
					TextInput.setText(TextInput.getText()+"0");
				}
				catch (Exception exc){
					MessageDialog.openError(shell, "Error", "Ops");
				}
			}
		});
		BtnNum0.setBounds(10, 200, 86, 35);
		
		Button buttonDel = new Button(shell, SWT.NONE);
		buttonDel.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try{
					TextInput.setText("");
					labelInput.setText("");
				}
				catch (Exception exc){
					MessageDialog.openError(shell, "Error", "Ops");
				}
			}
		});
		buttonDel.setBounds(160, 52, 60, 35);
		formToolkit.adapt(buttonDel, true, true);
		buttonDel.setText("\u0423\u0434\u0430\u043B\u0438\u0442\u044C");
		
		Button ButtonMinus = new Button(shell, SWT.NONE);
		ButtonMinus.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try{
					firstnum = TextInput.getText();
					TextInput.setText("");
					labelInput.setText(firstnum + " - ");
					operation = "-";
				}
				catch (Exception exc){
					MessageDialog.openError(shell, "Error", "Возможно вы не ввели число или оно слишком большое");
					
				}
			
			
			}
		});
		ButtonMinus.setBounds(160, 135, 60, 35);
		formToolkit.adapt(ButtonMinus, true, true);
		ButtonMinus.setText("-");
		
		Button ButtonMult = new Button(shell, SWT.NONE);
		ButtonMult.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try{
					firstnum = TextInput.getText();
					TextInput.setText("");
					labelInput.setText(firstnum + " * ");
					operation = "*";
				}
				catch (Exception exc){
					MessageDialog.openError(shell, "Error", "Возможно вы не ввели число или оно слишком большое");
					
				}
			}
		});
		ButtonMult.setBounds(160, 176, 60, 35);
		formToolkit.adapt(ButtonMult, true, true);
		ButtonMult.setText("*");
		
		Button ButtonDiv = new Button(shell, SWT.NONE);
		ButtonDiv.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try{
					firstnum = TextInput.getText();
					TextInput.setText("");
					labelInput.setText(firstnum + " / ");
					operation = "/";
				}
				catch (Exception exc){
					MessageDialog.openError(shell, "Error", "Возможно вы не ввели число или оно слишком большое");
					
				}
			}
		});
		ButtonDiv.setBounds(160, 217, 60, 35);
		formToolkit.adapt(ButtonDiv, true, true);
		ButtonDiv.setText("/");
		
		Button buttonPow = new Button(shell, SWT.NONE);
		buttonPow.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try{
					firstnum = TextInput.getText();
					TextInput.setText("");
					labelInput.setText(firstnum + " ^ ");
					operation = "^";
				}
				catch (Exception exc){
					MessageDialog.openError(shell, "Error", "Возможно вы не ввели число или оно слишком большое");
					
				}
			}
		});
		buttonPow.setBounds(224, 94, 60, 35);
		formToolkit.adapt(buttonPow, true, true);
		buttonPow.setText("^");
		
		Button buttonSqrt = new Button(shell, SWT.NONE);
		buttonSqrt.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try{
					if (Double.parseDouble(TextInput.getText()) > 0){
					firstnum = TextInput.getText();
					TextInput.setText("");
					operation = "sqrt";
					labelInput.setText("Sqrt(" + firstnum + ") =" + sqrtAndfactorial(firstnum, operation));
					TextInput.setText(sqrtAndfactorial(firstnum, operation));
					}
					else {labelInput.setText("Недопустимый ввод");}
				}
				catch (Exception exc){
					MessageDialog.openError(shell, "Error", "Возможно вы не ввели число или оно слишком большое");
					
				}
			}
		});
		buttonSqrt.setText("Sqrt");
		buttonSqrt.setBounds(224, 135, 60, 35);
		formToolkit.adapt(buttonSqrt, true, true);
		
		Button buttonFactorial = new Button(shell, SWT.NONE);
		buttonFactorial.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try{
					if (Double.parseDouble(TextInput.getText()) > 0) {
					firstnum = TextInput.getText();
					TextInput.setText("");
					operation = "!";
					labelInput.setText(firstnum + "! =" + sqrtAndfactorial(firstnum, operation));
					TextInput.setText(sqrtAndfactorial(firstnum, operation));
					}
					else {labelInput.setText("Недопустимый ввод");}
				}
				catch (Exception exc){
					MessageDialog.openError(shell, "Error", "Возможно вы не ввели число или оно слишком большое");
					
				}
			}
		});
		buttonFactorial.setBounds(224, 176, 60, 35);
		formToolkit.adapt(buttonFactorial, true, true);
		buttonFactorial.setText("!");
		
		Button buttonSin = new Button(shell, SWT.NONE);
		buttonSin.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try{
					firstnum = TextInput.getText();
					TextInput.setText("");
					operation = "sin";
					labelInput.setText("sin "+firstnum + " =" + sqrtAndfactorial(firstnum, operation));
					TextInput.setText(sqrtAndfactorial(firstnum, operation));
				}
				catch (Exception exc){
					MessageDialog.openError(shell, "Error", "Возможно вы не ввели число или оно слишком большое");
				}
			}
		});
		buttonSin.setBounds(288, 94, 60, 35);
		formToolkit.adapt(buttonSin, true, true);
		buttonSin.setText("Sin");
		
		Button buttonCos = new Button(shell, SWT.NONE);
		buttonCos.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try{
					firstnum = TextInput.getText();
					TextInput.setText("");
					operation = "cos";
					labelInput.setText("cos "+firstnum + " =" + sqrtAndfactorial(firstnum, operation));
					TextInput.setText(sqrtAndfactorial(firstnum, operation));
				}
				catch (Exception exc){
					MessageDialog.openError(shell, "Error", "Возможно вы не ввели число или оно слишком большое");
					
				}
			}
		});
		buttonCos.setBounds(290, 135, 60, 35);
		formToolkit.adapt(buttonCos, true, true);
		buttonCos.setText("Cos");
		
		Button buttonTg = new Button(shell, SWT.NONE);
		buttonTg.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try{
					firstnum = TextInput.getText();
					TextInput.setText("");
					operation = "tg";
					labelInput.setText("tg "+firstnum + " =" + sqrtAndfactorial(firstnum, operation));
					TextInput.setText(sqrtAndfactorial(firstnum, operation));
				}
				catch (Exception exc){
					MessageDialog.openError(shell, "Error", "Возможно вы не ввели число или оно слишком большое");
					
				}
			}
		});
		buttonTg.setText("Tg");
		buttonTg.setBounds(290, 176, 60, 35);
		formToolkit.adapt(buttonTg, true, true);
		
		Button buttonCtg = new Button(shell, SWT.NONE);
		buttonCtg.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try{
					firstnum = TextInput.getText();
					TextInput.setText("");
					operation = "ctg";
					labelInput.setText("ctg "+firstnum + " =" + sqrtAndfactorial(firstnum, operation));
					TextInput.setText(sqrtAndfactorial(firstnum, operation));
				}
				catch (Exception exc){
					MessageDialog.openError(shell, "Error", "Возможно вы не ввели число или оно слишком большое");
					
				}
			}
		});
		buttonCtg.setText("Ctg");
		buttonCtg.setBounds(290, 217, 60, 35);
		formToolkit.adapt(buttonCtg, true, true);
		
		Button btnScube = new Button(shell, SWT.NONE);
		btnScube.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try{
					if (Double.parseDouble(TextInput.getText()) > 0){
					firstnum = TextInput.getText();
					TextInput.setText("");
					operation = "areaOfCube5";
					labelInput.setText("S куба (ребро " + firstnum + ") =" + sqrtAndfactorial(firstnum, operation));
					TextInput.setText(sqrtAndfactorial(firstnum, operation));
					}
					else {labelInput.setText("Недопустимый ввод");}
				}
				catch (Exception exc){
					MessageDialog.openError(shell, "Error", "Возможно вы не ввели число или оно слишком большое");
					
				}
				
			}
		});
		btnScube.setBounds(354, 94, 60, 35);
		formToolkit.adapt(btnScube, true, true);
		btnScube.setText("S \u043A\u0443\u0431\u0430");
		
		Button btnVcube = new Button(shell, SWT.NONE);
		btnVcube.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try{
					if (Double.parseDouble(TextInput.getText()) > 0){
					firstnum = TextInput.getText();
					TextInput.setText("");
					operation = "volOfcube";
					labelInput.setText("V куба (ребро " + firstnum + ") =" + sqrtAndfactorial(firstnum, operation));
					TextInput.setText(sqrtAndfactorial(firstnum, operation));
					}
					else {labelInput.setText("Недопустимый ввод");}
				}
				catch (Exception exc){
					MessageDialog.openError(shell, "Error", "Возможно вы не ввели число или оно слишком большое");
					
				}
			}
		});
		btnVcube.setBounds(356, 135, 60, 35);
		formToolkit.adapt(btnVcube, true, true);
		btnVcube.setText("V \u043A\u0443\u0431\u0430");
		
		Button btnPcube = new Button(shell, SWT.NONE);
		btnPcube.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				try{
					if (Double.parseDouble(TextInput.getText()) > 0){
					firstnum = TextInput.getText();
					TextInput.setText("");
					operation = "perOfCube";
					labelInput.setText("P куба (ребро " + firstnum + ") =" + sqrtAndfactorial(firstnum, operation));
					TextInput.setText(sqrtAndfactorial(firstnum, operation));
					}
					else {labelInput.setText("Недопустимый ввод");}
				}
				catch (Exception exc){
					MessageDialog.openError(shell, "Error", "Возможно вы не ввели число или оно слишком большое");
					
				}
			}
		});
		btnPcube.setBounds(356, 176, 60, 35);
		formToolkit.adapt(btnPcube, true, true);
		btnPcube.setText("P \u043A\u0443\u0431\u0430");
		
		
		
		

	}
	protected DataBindingContext initDataBindings() {
		DataBindingContext bindingContext = new DataBindingContext();
		//
		return bindingContext;
	}
}
